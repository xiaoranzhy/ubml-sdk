/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.operation;

/**
 * The Definition Of Executinig Data Status For Determination or Validation
 *
 * @ClassName: ExecutingDataStatus
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ExecutingDataStatus {
    None(0),
    added(1),
    Modify(2),
    Deleted(4);

    private static java.util.HashMap<Integer, ExecutingDataStatus> mappings;
    private int intValue;

    private ExecutingDataStatus(int value) {
        intValue = value;
        ExecutingDataStatus.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, ExecutingDataStatus> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ExecutingDataStatus>();
        }
        return mappings;
    }

    public static ExecutingDataStatus forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}