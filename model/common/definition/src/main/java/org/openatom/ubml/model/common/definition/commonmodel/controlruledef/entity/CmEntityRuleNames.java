/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.controlruledef.entity;

public class CmEntityRuleNames {
    public static String CM_ENTITY_RULE_OBJECT_TYPE = "GspCommonModelEntity";
    public static String ADD_CHILD_ENTITY = "AddChildEntity";
    public static String MODIFY_CHILD_ENTITIES = "ModifyChildEntities";
}
