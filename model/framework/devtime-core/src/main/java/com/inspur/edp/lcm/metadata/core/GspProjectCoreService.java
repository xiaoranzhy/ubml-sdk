/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.common.configuration.GspProjectConfigurationHelper;
import java.io.IOException;
import java.util.List;

/**
 * @author zhaoleitr
 */
public class GspProjectCoreService {
    private final FileServiceImp fileService = new FileServiceImp();

    public GspProject getGspProjectInfo(String absolutePath) {
        if (absolutePath.isEmpty()) {
            throw new RuntimeException("路径不能为空");
        }
        String projPath = MetadataProjectCoreService.getCurrent().getProjPath(absolutePath);
        String fullName = fileService.getCombinePath(projPath, Utils.getGspProjectName());
        if (!fileService.isFileExist(fullName)) {
            throw new RuntimeException("文件" + fullName + "不存在");
        }
        String projStr = fileService.fileRead(Utils.handlePath(fullName));

        ObjectMapper objectMapper = Utils.getMapper();
        GspProject project;
        try {
            project = objectMapper.readValue(projStr, GspProject.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("序列化" + fullName + "失败", e);
        }
        return project;
    }

    public List<ProjectConfiguration> getGspProjectTypeInfo() {
        return GspProjectConfigurationHelper.getInstance().getGspProjectConfigurations();
    }
}
