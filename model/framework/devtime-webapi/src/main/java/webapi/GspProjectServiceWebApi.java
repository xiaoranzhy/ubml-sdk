/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi;

import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.GspProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class GspProjectServiceWebApi {
    private GspProjectService gspProjectService;

    private GspProjectService getGspProjectService() {
        if (gspProjectService == null) {
            gspProjectService = SpringBeanUtils.getBean(GspProjectService.class);
        }
        return gspProjectService;
    }

    /**
     * 获取工程信息（GSPProject.json的信息）
     *
     * @param path 工程路径
     * @return 工程路径
     * @throws IOException
     */
    @GET
    public GspProject getGspProjectInfo(@QueryParam(value = "path") String path) throws IOException {
        return getGspProjectService().getGspProjectInfo(path);
    }

    /**
     * 获取工程类型
     *
     * @return
     */
    @GET
    @Path("/type")
    public List<ProjectConfiguration> getGspProjectTypeInfo() {
        return getGspProjectService().getGspProjectTypeInfo();
    }

    /**
     * 更新工程基础技术栈
     *
     * @param path         工程路径
     * @param codeLanguage 技术栈
     * @Description 暂时不用
     */
    @PUT
    public void updateGspProject(@QueryParam(value = "path") String path,
        @QueryParam(value = "codeLanguage") String codeLanguage) {
        return;
    }
}
