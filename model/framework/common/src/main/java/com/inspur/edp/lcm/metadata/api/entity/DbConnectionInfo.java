/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity;

import io.iec.edp.caf.commons.dataaccess.DbType;

public class DbConnectionInfo {
    private String dbServerIP;
    private String dbPort;
    private String dbName;
    private String dbUserName;
    private String dbPassword;
    private DbType dbType;

    public String getDbServerIP() {
        return dbServerIP;
    }

    public void setDbServerIP(String dbServerIP) {
        this.dbServerIP = dbServerIP;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    private String url;

    public String getUrl() {
        if (url == null || url.length() <= 0) {
            switch (dbType) {
                case DM:
                    url = "jdbc:dm://" + getDbServerIP() + ":" + dbPort + "/DMSERVER?schema=" + dbName;
                    break;
                case PgSQL:
                    url = "jdbc:postgresql://" + getDbServerIP() + ":" + dbPort + "/" + dbName;
                    break;
                case Oracle:
                    url = "jdbc:oracle:thin:@//" + getDbServerIP() + ":" + dbPort + "/" + dbName;
                    break;
                case SQLServer:
                    url = "jdbc:sqlserver://" + getDbServerIP() + "\\" + dbName + ":" + dbPort + ";database=" + dbName;
                    break;
                case HighGo:
                    url = "jdbc:highgo://" + getDbServerIP() + ":" + dbPort + "/" + dbName;
                    break;
                case MySQL:
                    url = "jdbc:mysql://" + getDbServerIP() + ":" + dbPort + "/" + dbName + "?characterEncoding=utf8&serverTimezone=UTC&useSSL=false";
                    break;
                case Oscar:
                    url = "jdbc:oscar://" + getDbServerIP() + ":" + dbPort + "/" + dbName;
                    break;
                default:
                    throw new RuntimeException("数据库类型不正确");
            }
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
