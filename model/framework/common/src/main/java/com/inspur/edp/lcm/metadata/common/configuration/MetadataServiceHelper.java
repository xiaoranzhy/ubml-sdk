/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;
import com.inspur.edp.lcm.metadata.api.entity.MetadataType;
import com.inspur.edp.lcm.metadata.api.entity.compiler.MetadataCompilerConfiguration;
import com.inspur.edp.lcm.metadata.api.entity.extract.ExtractConfigration;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

/**
 * @Classname MetadataServiceHelper
 * @Description 元数据服务工具
 * @Date 2019/7/20 14:20
 * @Created by zhongchq
 * @Version 1.0
 */
public class MetadataServiceHelper {
    ObjectMapper objectMapper = new ObjectMapper();
    String fileContents;
    private static FileServiceImp fileService = new FileServiceImp();

    //默认构造器
    public MetadataServiceHelper() {
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    public List<MetadataConfiguration> getMetadataConfigurationList(String path,
        String sectionName) throws IOException {
        fileContents = fileService.fileRead(path);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<List<MetadataConfiguration>>() {
        });
    }

    public List<MetadataType> getMetadataTypeList(String path, String sectionName) throws IOException {
        fileContents = fileService.fileRead(path);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<List<MetadataType>>() {
        });
    }

    public List<ExtractConfigration> getExtractConfigrationList(String fileName,
        String sectionName) throws IOException {
        fileContents = fileService.fileRead(fileName);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<List<ExtractConfigration>>() {
        });
    }

    public List<MetadataCompilerConfiguration> getCompileConfigrationList(String fileName,
        String sectionName) throws IOException {
        fileContents = fileService.fileRead(fileName);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<List<MetadataCompilerConfiguration>>() {
        });
    }

    public String getMavenProjectName(String gspProjectExtend) throws IOException {
        fileContents = fileService.fileRead(gspProjectExtend);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        return jsonNode.findValue("MavenProjectName").asText();
    }

    public Map<String, List<String>> getMavenSourceRule(String fileName, String sectionName) throws IOException {
        fileContents = fileService.fileRead(fileName);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<Map<String, List<String>>>() {
        });
    }

    public String getEdpParentVersion(String pomPath) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(pomPath);
            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(fis);
            return model.getParent().getVersion();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (!(null == fis)) {
                fis.close();
            }
        }
        return null;
    }

    public List<ProjectConfiguration> getProjectConfiguration(String path,
        String sectionName) throws IOException {
        fileContents = fileService.fileRead(path);
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String metadataConfiguration = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(metadataConfiguration, new TypeReference<List<ProjectConfiguration>>() {
        });
    }
}
